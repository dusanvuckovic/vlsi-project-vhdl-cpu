library ieee;
use ieee.std_logic_1164.all;
use work.datatypes_package.all;

entity cpu is	
	port (
		reset_cpu: in std_logic;
		clk_cpu: in std_logic;
		end_cpu: out std_logic;
		
		imem_key: out insmem_key;
		imem_value: in insmem_value;
		
		dmem_key: out datamem_key;
		dmem_value: in datamem_value							
	);
end cpu;

architecture cpu_impl of cpu is

signal if_id_cpu: interphase := initialize_interphase;
signal id_ex_cpu: interphase := initialize_interphase;
signal ex_mem_cpu: interphase := initialize_interphase;
signal mem_wb_cpu: interphase := initialize_interphase;

signal if_predict_value_cpu: prediction_value;
signal if_predict_key_cpu: prediction_key;
signal ex_mem_predict_upd_cpu: prediction_update;

signal fwd_res_cpu: forwarded_results;

signal if_id_fw_cpu: id_forward;
signal id_ex_fw_cpu: exmemwb_forward;
signal ex_mem_fw_cpu: exmemwb_forward;
signal mem_wb_fw_cpu: exmemwb_forward;

signal ex_flush_base_cpu: std_logic := '0';
signal mem_flush_base_cpu: std_logic := '0';
signal id_flush_res_cpu: std_logic := '0';
signal ex_flush_res_cpu: std_logic := '0';
signal mem_flush_res_cpu: std_logic := '0';

signal jmpjsrbr_addr_cpu: jumpjsrbr_address;
signal rts_addr_cpu: rts_address;

signal end_cpu_int: std_logic;

begin		
	
	ifetch: entity work.instruction_fetch(instruction_fetch_impl)	
		port map (
			clk_if => clk_cpu,
			reset_if => reset_cpu,
			end_if => end_cpu_int,
							
			imem_in => imem_key,
			imem_out => imem_value,
			
			if_id_out => if_id_cpu,
			if_id_fw_out => if_id_fw_cpu,
			if_stall => fwd_res_cpu.stall,
			
			if_predict_outp => if_predict_key_cpu,
			if_jmpjsrbr_addr => jmpjsrbr_addr_cpu,
			if_rts_addr => rts_addr_cpu,
			if_predict_inp => if_predict_value_cpu
	);	
	
	idecode: entity work.instruction_decode(instruction_decode_impl)
		port map (
			clk_id => clk_cpu,
			reset_id => reset_cpu,
			
			id_stall_in => fwd_res_cpu.stall,
			id_flush_in => id_flush_res_cpu,
			if_id_in => if_id_cpu,
			wb_in => mem_wb_cpu,
			
			id_ex_out => id_ex_cpu,
			
			id_ex_fw_out => id_ex_fw_cpu,
			fw_res_in => fwd_res_cpu
	);
	
	exec: entity work.execute(execute_impl)
		port map (
			clk_ex => clk_cpu,
			reset_ex => reset_cpu,
			
			ex_flush_in => ex_flush_res_cpu,
			ex_flush_out => ex_flush_base_cpu,
			id_ex_in => id_ex_cpu,	
			ex_mem_out => ex_mem_cpu,
			
			ex_mem_jmpjsrbr_out => jmpjsrbr_addr_cpu,	
			ex_mem_predict_upd => ex_mem_predict_upd_cpu,		
			ex_mem_fw_out => ex_mem_fw_cpu
	);
	
	mem: entity work.memory(memory_impl)
		port map (
			clk_mem => clk_cpu,
			reset_mem => reset_cpu,
			end_mem => end_cpu,
			end_mem_int => end_cpu_int,
			
			ex_mem_in => ex_mem_cpu,
			mem_wb_out => mem_wb_cpu,
									
			mem_flush_in => mem_flush_res_cpu,
			mem_flush_out => mem_flush_base_cpu,
			
			dmem_in => dmem_value,
			dmem_out => dmem_key,			
			
			mem_wb_rts_out => rts_addr_cpu,
			mem_wb_fw_out => mem_wb_fw_cpu
	);
	
	fwd: entity work.forward(forward_impl)
		port map (
			clk_fw => clk_cpu,
			reset_fw => reset_cpu,
			
			ex_flush_base => ex_flush_base_cpu,
			mem_flush_base => mem_flush_base_cpu,
			
			if_id_fw_in => if_id_fw_cpu,
			id_ex_fw_in => id_ex_fw_cpu,
			ex_mem_fw_in => ex_mem_fw_cpu,
			mem_wb_fw_in => mem_wb_fw_cpu,
			
			results_fw_out => fwd_res_cpu		
	);
	
	predict: entity work.predict(predict_impl)
		port map (
		clk_pred => clk_cpu,
		reset_pred => reset_cpu,		
				
		if_pred_inp => if_predict_key_cpu,
		if_pred_outp => if_predict_value_cpu,
		ex_mem_pred_upd => ex_mem_predict_upd_cpu		
	);
	
	flsh: entity work.flush(flush_impl)		
	port map (
		clk_flush => clk_cpu,		
		
		ex_flush_base => ex_flush_base_cpu,
		mem_flush_base => mem_flush_base_cpu,		
		id_flush_res => id_flush_res_cpu,
		ex_flush_res => ex_flush_res_cpu,
		mem_flush_res => mem_flush_res_cpu 		
	);
				
end cpu_impl;