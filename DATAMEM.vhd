library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.datatypes_package.all;

entity data_memory is
	port (	
		--cpu_end: 
		dmem_in: in datamem_key;
		dmem_out: out datamem_value;
		dmem_end: in std_logic
	);
end data_memory;

architecture data_memory_impl of data_memory is

	constant DATAMEM_SIZE: integer := 2**16;
	
	type DATA_MEMORY is array (0 to DATAMEM_SIZE-1) of word;
	type DIRTY_BITS is array (0 to DATAMEM_SIZE-1) of std_logic;
	type file_read is file of character;
	
	function init_ram(filename: string) return DATA_MEMORY is	
	file file_pointer : text;
	variable line_read : line;
	variable address, data: word;
	variable ram_to_return : DATA_MEMORY;	
	variable bvector: bit_vector(31 downto 0);
	begin	
		file_open(file_pointer, filename, READ_MODE);   
		for i in DATA_MEMORY'range loop	
			ram_to_return(i) := X"ffffffff";
		end loop;
		
		for i in DATA_MEMORY'range loop				   
		
			if endfile(file_pointer) then 
				exit;
				end if;
			
			readline(file_pointer, line_read);
			
			if (line_read(1) = '*') or (line_read(1) = '/') or (line_read(1) = HT) then
				next;			
			end if;
			
			hread(line_read, address);
			
			read(line_read, bvector);
			data := to_stdlogicvector(bvector);
					
			ram_to_return(to_integer(unsigned(address))) := data;
			
		end loop;	
		file_close(file_pointer);
		return ram_to_return;
	end function;
	
	function init_bits return DIRTY_BITS is		
	variable bits_to_return : DIRTY_BITS;		
	begin			
		for i in DATA_MEMORY'range loop	
			bits_to_return(i) := '0';
		end loop;			
		return bits_to_return;
	end function;

	signal datamem: DATA_MEMORY := init_ram(data_in_test);
	signal datamem_dirty: DIRTY_BITS := init_bits;
	shared variable mem_done: std_logic := '0';                                    
	
begin

	process(dmem_in.clk)
	variable addr_datamem_old: std_logic_vector(31 downto 0);
	variable init : boolean := false;	
	begin						
		if init then						
			if (dmem_in.rd = '1') then
				dmem_out.data <= datamem(to_integer(unsigned(dmem_in.addr)));
			elsif (dmem_in.wr = '1') then				
				datamem(to_integer(unsigned(dmem_in.addr))) <= dmem_in.data;						
				datamem_dirty(to_integer(unsigned(dmem_in.addr))) <= '1';
			end if;			
			dmem_out.done <= mem_done;
			mem_done := not mem_done;
		else			
			init := true;
		end if;
	end process;	
	
	process(dmem_end)
	file file_pointer: text;
	variable address, data_file, data_mem: word;
	variable bvector: bit_vector(31 downto 0);
	variable line_read : line;
	variable valid: boolean := true;
	begin		
		if (dmem_end = '1') then
			file_open(file_pointer, data_out_test, READ_MODE);   
			
			for i in DATA_MEMORY'range loop				   
		
				if endfile(file_pointer) then 
					exit;
				end if;
			
				readline(file_pointer, line_read);
			
				if (line_read(1) = '*') or (line_read(1) = '/') or (line_read(1) = HT) then
					next;			
				end if;
			
				hread(line_read, address);				
			
				read(line_read, bvector);
				data_file := to_stdlogicvector(bvector);
				
				data_mem := datamem(to_integer(unsigned(address)));
				
				if (data_mem /= data_file) then
					report "Nepodudaranje memorije!" severity error;									
					valid := false;
				end if;
					
			end loop;				
			if (valid) then
				report "Kraj rada bez problema!" severity failure;
			end if;
		end if;			
			
	end process;

end data_memory_impl;
