library ieee;
use ieee.std_logic_1164.all;
 
package datatypes_package is

constant inst_in_test: string := "c:\altera\13.0sp1\test3_inst_in.txt";
constant data_in_test: string := "c:\altera\13.0sp1\test3_data_in.txt";
constant data_out_test: string := "c:\altera\13.0sp1\test3_data_out.txt";

subtype word is std_logic_vector(31 downto 0);
subtype half_word is std_logic_vector(15 downto 0);
subtype opcode_bits is std_logic_vector(5 downto 0);
subtype reg_num is std_logic_vector(4 downto 0);
subtype rot_const is std_logic_vector(4 downto 0);

--MEMORIJSKI PODACI--
type insmem_key is
record
	clk: std_logic;
	rd: std_logic;
	addr: word;
end record;

type insmem_value is
record
	done: std_logic;	
	data: word;
end record;

type datamem_key is
record
	clk: std_logic;
	rd, wr: std_logic;
	addr, data: word;
end record;

type datamem_value is
record
	done: std_logic;	
	data: word;
end record;
--MEMORIJSKI PODACI--


--PREDIKCIONI PODACI 
type prediction_key is
record
	enable: std_logic;
	pc: word;
end record;

type prediction_value is
record
	pred_made: std_logic;
	pred_taken: std_logic;	
	pc_pred: word;
end record;
	
type prediction_update is
record	
	pred_ins: std_logic;
	pred_made: std_logic;
	pred_taken: std_logic;
	pred_calc: std_logic;	
	pc_entry: word;
	pc_pred: word;
	pc_calc: word;
end record;
--PREDIKCIONI PODACI
 
type instruction is (err, load_ins, store_ins, mov_ins, movi_ins, add_ins, sub_ins, addi_ins, subi_ins, 
	and_ins, or_ins, xor_ins, not_ins, shl_ins, shr_ins, sar_ins, rol_ins, ror_ins, 
	jmp_ins, jsr_ins, rts_ins, push_ins, pop_ins, 
	beq_ins, bnq_ins, bgt_ins, blt_ins, bge_ins, ble_ins, halt_ins
);	

--PODACI ZA PROSLEĐIVANJE
type id_forward is
record
	enable: std_logic;
	rs1_num, rs2_num: reg_num;
end record;

type forwarded_results is
record
	stall: std_logic;
	rs1_fwd, rs2_fwd: std_logic;
	rs1, rs2: word;

end record;
type exmemwb_forward is
record
	stall: std_logic;
	rd_num: reg_num;
	rd_used: std_logic;
	rd_done: std_logic;
	rd: word;
end record;
--PODACI ZA PROSLEĐIVANJE

type jumpjsrbr_address is
record
	is_jump: std_logic;
	jaddr : word;
end record;

type rts_address is
record
	is_rts: std_logic;
	rts : word;
end record;


type interphase is
record
	valid: std_logic;
	flush: std_logic;

	ir: word;
	pc, pc_next, pc_pred: word;
	pred_ins, pred_made, pred_taken, pred_calc: std_logic;
	
	opcode: instruction;	
	rd_num, rs1_num, rs2_num: reg_num;
	imm: word;
	imm_rot: rot_const;	
	rd, rs1, rs2: word;
	rd_used, rd_done : std_logic;
	
end record;

function initialize_interphase return interphase;
function get_forward(ins: interphase) return exmemwb_forward;
function get_blocked_forward return exmemwb_forward;
function get_no_jmpjsrbr return jumpjsrbr_address;
function initialize_forwarded_results return forwarded_results;
function get_no_rts return rts_address;

end package datatypes_package;

package body datatypes_package is

function initialize_interphase return interphase is
	variable ip: interphase;
	begin		
		ip.valid:= '0';
		ip.flush := '0';
		ip.ir := X"00000000";
		ip.pc := X"00000000";
		ip.pc_next := X"00000000";
		ip.pc_pred := X"00000000";
		ip.pred_ins := '0';
		ip.pred_made := '0';
		ip.pred_taken := '0';
		ip.pred_calc := '0';
		ip.opcode := err;
		ip.rd_num := B"00000";
		ip.rs1_num := B"00000";
		ip.rs2_num := B"00000";
		ip.imm := X"00000000";
		ip.imm_rot := B"00000";
		ip.rd := X"00000000";
		ip.rs1 := X"00000000";
		ip.rs2 := X"00000000";
		ip.rd_used := '0';
		ip.rd_done := '0';
		return ip;
	end function initialize_interphase;
	
function get_blocked_forward return exmemwb_forward is
	variable fw: exmemwb_forward;
begin	
	fw.stall := '1';
	fw.rd_num := B"11111";
	fw.rd_used := '0';
	fw.rd_done := '0';
	fw.rd := X"FFFFFFFF";
	return fw;
end function get_blocked_forward;
	
function get_forward(ins: interphase) return exmemwb_forward is
	variable fw: exmemwb_forward;
begin	
	fw.stall := ins.flush;
	fw.rd_num := ins.rd_num;
	fw.rd_used := ins.rd_used;
	fw.rd_done := ins.rd_done;
	fw.rd := ins.rd;
	return fw;
end function get_forward;
	
function initialize_forwarded_results return forwarded_results is
	variable fr: forwarded_results;
begin
	fr.stall := '0';
	fr.rs1_fwd := '0';
	fr.rs2_fwd := '0';
	fr.rs1 := X"FFFFFFFF";
	fr.rs2 := X"FFFFFFFF";
	return fr;
end function initialize_forwarded_results;

function get_no_jmpjsrbr return jumpjsrbr_address is
	variable ja: jumpjsrbr_address;
begin
	ja.jaddr := X"FFFFFFFF";
	ja.is_jump := '0';
	return ja;
end function get_no_jmpjsrbr;

function get_no_rts return rts_address is
	variable rts: rts_address;
begin
	rts.is_rts := '0';
	rts.rts := X"FFFFFFFF";
	return rts;
end function get_no_rts;

end package body;   				