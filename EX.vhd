library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.datatypes_package.all;

entity execute is
	port
	(
		clk_ex: in std_logic;
		reset_ex: in std_logic;		
					
		ex_flush_in: in std_logic;
		ex_flush_out: out std_logic;
		id_ex_in: in interphase;
		ex_mem_out: out interphase;
		
		ex_mem_jmpjsrbr_out: out jumpjsrbr_address;
		ex_mem_predict_upd: out prediction_update;
		ex_mem_fw_out: out exmemwb_forward
	);
	
end execute;

architecture execute_impl of execute is

begin
	process (clk_ex)
	variable adder_res, and_res, or_res, xor_res, not_res, shift_res: std_logic_vector(31 downto 0);
	variable opcode: instruction;
	variable rd, rs1, rs2, imm: word;
	variable imm_rot: rot_const;
	variable gt, eq, lt, ge, nq, le: boolean := false;	
	variable pred_ins: boolean := false;
	variable branch_taken: std_logic := '0';
	
	variable rotate_by: integer;
	variable ex_mem : interphase;
	variable op1, op2: signed(31 downto 0);
	begin
		if rising_edge(clk_ex) then
			if (id_ex_in.valid = '0' or id_ex_in.flush = '1' or ex_flush_in = '1') then
				ex_mem_out <= initialize_interphase;
				ex_mem_out.valid <= '0';
				ex_flush_out <= '0';
				ex_mem_fw_out <= get_blocked_forward;
				ex_mem_jmpjsrbr_out <= get_no_jmpjsrbr;	
				
				ex_mem_predict_upd.pred_ins <= '0';
				ex_mem_predict_upd.pred_made <= '0';
				ex_mem_predict_upd.pred_taken <= '0';
				ex_mem_predict_upd.pred_calc <= '0';
				ex_mem_predict_upd.pc_entry <= X"FFFFFFFF";
				ex_mem_predict_upd.pc_pred <= X"FFFFFFFF";
				ex_mem_predict_upd.pc_calc <= X"FFFFFFFF";
				
			else		
				opcode := id_ex_in.opcode;
				rd := id_ex_in.rd;
				rs1 := id_ex_in.rs1;
				rs2 := id_ex_in.rs2;
				imm := id_ex_in.imm;
				imm_rot := id_ex_in.imm_rot;
			
				--je li instrukcija skoka
				pred_ins := (opcode = beq_ins) or (opcode = bnq_ins) or (opcode = blt_ins) or 
				(opcode = bgt_ins) or (opcode = ble_ins) or (opcode = bge_ins);				
				--je li instrukcija skoka
			
				--sabirač				
				case opcode is
					when add_ins => op1 := signed(rs1); op2 := signed(rs2);
					when sub_ins => op1 := signed(rs1);	op2 := -signed(rs2);
					when addi_ins | load_ins | store_ins | jmp_ins | jsr_ins => op1 := signed(rs1); op2 := signed(imm);										
					when subi_ins => op1 := signed(rs1); op2 := -signed(imm);
					when others => op1 := signed(id_ex_in.pc_next); op2 := signed(imm); --branch
				end case;																					
				adder_res := std_logic_vector(op1 + op2);
				--sabirač
				
				--logičke
				and_res := rs1 and rs2;
				or_res := rs1 or rs2;
				xor_res := rs1 xor rs2;
				not_res := not rs1;
				--logičke
			
				--pomeračke
				rotate_by := to_integer(unsigned(imm_rot));
				case opcode is
					when shl_ins
						=> shift_res := std_logic_vector(unsigned(rd) sll rotate_by);
					when shr_ins
						=> shift_res := std_logic_vector(unsigned(rd) srl rotate_by);
					when sar_ins
						=> shift_res := to_stdlogicvector(to_bitvector(rd) sra rotate_by);
					when rol_ins
						=> shift_res := std_logic_vector(unsigned(rd) rol rotate_by);
					when ror_ins
						=> shift_res := std_logic_vector(unsigned(rd) ror rotate_by);
					when others
						=> shift_res := X"00000000";
				end case;
	
				--pomeračke
			
				--uslovi skoka
				gt := signed(rs1) > signed(rs2);
				eq := signed(rs1) = signed(rs2);
				lt := signed(rs1) < signed(rs2);				
				le := not gt;
				nq := not eq;
				ge := not lt;				
				--uslovi skoka			
			
				--ima li skoka									
				if ((opcode = beq_ins) and eq) or ((opcode = bnq_ins) and nq) or ((opcode = blt_ins) and lt)
				or ((opcode = bgt_ins) and gt) or ((opcode = ble_ins) and le) or ((opcode = bge_ins) and ge) then				
					branch_taken := '1';
				else
					branch_taken := '0';
				end if;
				--ima li skoka
			
				--uneti na kraj
				ex_mem := id_ex_in;
												
				if (ex_mem.rd_done = '0') then
					case opcode is
					when shl_ins | shr_ins | sar_ins | rol_ins | ror_ins 
						=> ex_mem.rd := shift_res;
					when and_ins 
						=> ex_mem.rd := and_res;
					when or_ins 
						=> ex_mem.rd := or_res;
					when xor_ins 
						=> ex_mem.rd := xor_res;
					when not_ins 
						=> ex_mem.rd := not_res;
					when jsr_ins
						=> ex_mem.rd := ex_mem.pc_next;
					when others
						=> ex_mem.rd := adder_res;
					end case;
				end if;
				
				if (opcode = add_ins) or (opcode = sub_ins) or (opcode = addi_ins) 
				or (opcode = store_ins) or (opcode = jmp_ins) or (opcode = jsr_ins) 
				or (opcode = subi_ins) or (opcode = and_ins) or (opcode = or_ins) 
				or (opcode = xor_ins) or (opcode = not_ins) or (opcode = shl_ins) 
				or (opcode = shr_ins) or (opcode = sar_ins) or (opcode = rol_ins) 
				or (opcode = ror_ins) then
					ex_mem.rd_done := '1';									
				end if;
					
			--uneti na kraj			
			if (opcode = jmp_ins) or (opcode = jsr_ins) or 
			(
				pred_ins and branch_taken = '1' and (
					id_ex_in.pred_made = '0' or (id_ex_in.pred_made = '1' and id_ex_in.pred_taken = '0')					
				)
			)	then
					ex_mem_jmpjsrbr_out.jaddr <= adder_res;
					ex_mem_jmpjsrbr_out.is_jump <= '1';
					ex_flush_out <= '1';				
			end if;
			if pred_ins and branch_taken = '0' and id_ex_in.pred_made = '1' and id_ex_in.pred_taken = '1' then
				ex_mem_jmpjsrbr_out.jaddr <= id_ex_in.pc_next;
				ex_mem_jmpjsrbr_out.is_jump <= '1';
				ex_flush_out <= '1';				
			end if;
			if (ex_flush_in = '1') then
				ex_mem.flush := '1';
			end if;
			if (pred_ins) then
				ex_mem.pred_ins := '1';		
			end if;
			if (branch_taken = '1') then
				ex_mem.pred_calc := '1';
			end if;
						
			ex_mem_out <= ex_mem;
			ex_mem_fw_out <= get_forward(ex_mem);	
			
			ex_mem_predict_upd.pred_ins <= ex_mem.pred_ins;
			ex_mem_predict_upd.pred_made <= ex_mem.pred_made;
			ex_mem_predict_upd.pred_taken <= ex_mem.pred_taken;
			ex_mem_predict_upd.pred_calc <= ex_mem.pred_calc;
			ex_mem_predict_upd.pc_entry <= ex_mem.pc;
			ex_mem_predict_upd.pc_pred <= ex_mem.pc_pred;			
			ex_mem_predict_upd.pc_calc <= ex_mem.rd;
			
			end if;							
			
		end if;	
	end process;
	
end execute_impl;