library ieee;
use ieee.std_logic_1164.all;
use work.datatypes_package.all;

entity flush is	
	port (
		clk_flush: in std_logic;
	
		ex_flush_base: in std_logic;
		mem_flush_base: in std_logic;
		
		id_flush_res: out std_logic;
		ex_flush_res: out std_logic;
		mem_flush_res: out std_logic
	);
end flush;

architecture flush_impl of flush is	
begin
	process(clk_flush) 
	begin
		if falling_edge(clk_flush) then
			if (ex_flush_base = '1' or mem_flush_base = '1') then
				id_flush_res <= '1';
				ex_flush_res <= '1';
			else
				id_flush_res <= '0';
				ex_flush_res <= '0';
			end if;		
		
			if (mem_flush_base = '1') then
				mem_flush_res <= '1';
			else
				mem_flush_res <= '0';
			end if;
		end if;
	end process;
	
end flush_impl;