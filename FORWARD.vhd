library ieee;
use ieee.std_logic_1164.all;
use work.datatypes_package.all;

entity forward is	
	port (
		clk_fw: in std_logic;
		reset_fw: in std_logic;	
		
		ex_flush_base: in std_logic;
		mem_flush_base: in std_logic;
	
		if_id_fw_in: in id_forward;
		id_ex_fw_in: in exmemwb_forward;
		ex_mem_fw_in: in exmemwb_forward;
		mem_wb_fw_in: in exmemwb_forward;
		results_fw_out: out forwarded_results
	);
end forward;

architecture forward_impl of forward is

	function is_fw_needed(rs_num: reg_num; stage_fw: exmemwb_forward) return boolean is
	variable is_ok: boolean := false;
	begin
		if (stage_fw.stall = '0') and (stage_fw.rd_num = rs_num) and (stage_fw.rd_used = '1') then
			is_ok := true;
		end if;
	return is_ok;
	end function is_fw_needed;
	
begin
	
	process(clk_fw)		
		variable need_fw_ex_rs1, need_fw_mem_rs1, need_fw_wb_rs1: boolean := false;
		variable need_fw_ex_rs2, need_fw_mem_rs2, need_fw_wb_rs2: boolean := false;
		variable can_fw_ex_rs1, can_fw_mem_rs1, can_fw_wb_rs1: boolean := false;
		variable can_fw_ex_rs2, can_fw_mem_rs2, can_fw_wb_rs2: boolean := false;
		
		variable fr: forwarded_results;
	begin				
		if falling_edge(clk_fw) then		
			fr := initialize_forwarded_results;					
			if (if_id_fw_in.enable = '1' and reset_fw /= '1' and ex_flush_base /= '1' and mem_flush_base /= '1') then
			--dohvati jesu li potrebni fw-ovi
			need_fw_ex_rs1 := is_fw_needed(if_id_fw_in.rs1_num, id_ex_fw_in);		
			need_fw_ex_rs2 := is_fw_needed(if_id_fw_in.rs2_num, id_ex_fw_in);
			need_fw_mem_rs1 := is_fw_needed(if_id_fw_in.rs1_num, ex_mem_fw_in);
			need_fw_mem_rs2 := is_fw_needed(if_id_fw_in.rs2_num, ex_mem_fw_in);
			need_fw_wb_rs1 := is_fw_needed(if_id_fw_in.rs1_num, mem_wb_fw_in);
			need_fw_wb_rs2 := is_fw_needed(if_id_fw_in.rs2_num, mem_wb_fw_in);
			--dohvati jesu li potrebni fw-ovi
			
			--proveri je li neophodno zaustavljanje
			if (need_fw_ex_rs1 or need_fw_ex_rs2) and id_ex_fw_in.rd_done = '0' then
				fr.stall := '1';
			elsif (need_fw_mem_rs1 or need_fw_mem_rs2) and ex_mem_fw_in.rd_done = '0' then
				fr.stall := '1';			
			end if;					
			--proveri je li neophodno zaustavljanje
						
			--prosledi vrednosti rs1
			if need_fw_ex_rs1 or need_fw_mem_rs1 or need_fw_wb_rs1 then
				fr.rs1_fwd := '1';
				if need_fw_ex_rs1 then 									
					fr.rs1 := id_ex_fw_in.rd;
				elsif need_fw_mem_rs1 then
					fr.rs1 := ex_mem_fw_in.rd;
				else
					fr.rs1 := mem_wb_fw_in.rd;
				end if;
			end if;
			--prosledi vrednosti rs1
				
			--prosledi vrednosti rs2
			if need_fw_ex_rs2 or need_fw_mem_rs2 or need_fw_wb_rs2 then
				fr.rs2_fwd := '1';
				if need_fw_ex_rs2 then 
					fr.rs2 := id_ex_fw_in.rd;
				elsif need_fw_mem_rs2 then
					fr.rs2 := ex_mem_fw_in.rd;
				else
					fr.rs2 := mem_wb_fw_in.rd;
				end if;
			end if;
			--prosledi vrednosti rs2			
			end if;						
			--dodaj u izlaz za fw	
			results_fw_out <= fr;
			--dodaj u izlaz za fw
		end if;
																						
	end process;
	
end forward_impl;