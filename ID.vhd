library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.datatypes_package.all;

entity instruction_decode is
	port
	(
		clk_id: in std_logic;
		reset_id: in std_logic;	
		
		id_stall_in: in std_logic;
		id_flush_in: in std_logic;
		if_id_in: in interphase;
		wb_in: in interphase;
		
		id_ex_out: out interphase;
		
		id_ex_fw_out: out exmemwb_forward;				
		fw_res_in: in forwarded_results
	);
	
end instruction_decode;

architecture instruction_decode_impl of instruction_decode is
	
	constant REG_FILE_SIZE: integer := 32;
	
	type REGISTER_FILE is array (0 to REG_FILE_SIZE-1) of word;	

function init_rfile return REGISTER_FILE is
	variable rf: REGISTER_FILE;
begin
	for i in rf'low to rf'high loop
		rf(i) := X"FFFFFFFF";
	end loop;
	return rf;
end function;	
	
signal rfile: REGISTER_FILE := init_rfile;
	
function determine_instruction(opcode: std_logic_vector(5 downto 0)) return instruction is
	variable ret: instruction;	
begin
	case opcode is
		when B"000000" => ret := load_ins;
		when B"000001" => ret := store_ins;
		when B"000100" => ret := mov_ins;
		when B"000101" => ret := movi_ins;
		
		when B"001000" => ret := add_ins;
		when B"001001" => ret := sub_ins;
		when B"001100" => ret := addi_ins;		
		when B"001101" => ret := subi_ins;
		
		when B"010000" => ret := and_ins;
		when B"010001" => ret := or_ins;
		when B"010010" => ret := xor_ins;
		when B"010011" => ret := not_ins;
		
		when B"011000" => ret := shl_ins;
		when B"011001" => ret := shr_ins;
		when B"011010" => ret := sar_ins;
		when B"011011" => ret := rol_ins;
		when B"011100" => ret := ror_ins;
		
		when B"100000" => ret := jmp_ins;
		when B"100001" => ret := jsr_ins;
		when B"100010" => ret := rts_ins;
		when B"100100" => ret := push_ins;
		when B"100101" => ret := pop_ins;
		
		when B"101000" => ret := beq_ins;
		when B"101001" => ret := bnq_ins;
		when B"101010" => ret := bgt_ins;
		when B"101011" => ret := blt_ins;
		when B"101100" => ret := bge_ins;
		when B"101101" => ret := ble_ins;
		
		when B"111111" => ret := halt_ins;
		
		when others => ret := err;
	end case;

	return ret;	
end function;
	
begin
	process(clk_id)
			
	variable ir: word;	
	variable opcode: instruction;
	variable opcode_num: opcode_bits;
	variable rd_num, rs1_num, rs2_num: reg_num;	
	variable imm_rot: rot_const;
	variable imm_ldarlogj: word;
	variable imm_stbr: word;	
	variable helper: half_word;
	
	variable arlog_i, arlog_ni: boolean := false;
	variable shrot: boolean := false;
	variable branch: boolean := false;
	variable rd_used, rs1_used, rs2_used : boolean := false;
	variable rd_operation_done : boolean := false;
	
	variable rd, rs1, rs2: word;
	
	variable id_ex: interphase := initialize_interphase;
	begin
		if (rising_edge(clk_id)) then
			if (if_id_in.valid = '0' or id_flush_in = '1' or id_stall_in = '1') then
				id_ex_out <= initialize_interphase;
				id_ex_out.valid <= '0';
				id_ex_fw_out <= get_blocked_forward;
			else				
				ir := if_id_in.ir;
				--decode instruction
				opcode_num := ir(31 downto 26);
				rd_num := ir(25 downto 21);
				rs1_num := ir(20 downto 16);
				rs2_num := ir(15 downto 11);
				imm_rot := ir(15 downto 11);
				opcode := determine_instruction(opcode_num);
				--decode instruction
						
				--signext immediate values
				imm_ldarlogj := std_logic_vector(resize(signed(ir(15 downto 0)), imm_ldarlogj'length));
				helper := ir(25 downto 21) & ir(10 downto 0);
				imm_stbr := std_logic_vector(resize(signed(helper), imm_stbr'length));
				--signext immediate values
					
				--set branch, shrot
				shrot := (opcode = shl_ins) or (opcode = shr_ins) or (opcode = sar_ins) or
					(opcode = rol_ins) or (opcode = ror_ins);				
				branch := (opcode = beq_ins) or (opcode = bnq_ins) or (opcode = bgt_ins) or
					(opcode = blt_ins) or (opcode = bge_ins) or (opcode = ble_ins);				
				--set branch, shrot
						
				--rd_used
				rd_used := not ((opcode = store_ins) or (opcode = jmp_ins) or (opcode = jsr_ins) or 
					branch or (opcode = halt_ins) or (opcode = push_ins));				
				--rd_used
			
				--dohvati rs1 i rs2
				if (fw_res_in.rs1_fwd = '1') then 
					if (shrot or opcode = movi_ins) then
						rd := fw_res_in.rs1;
					else
						rs1 := fw_res_in.rs1;
					end if;
				else
					if (shrot or opcode = movi_ins) then
						rd := rfile(to_integer(unsigned(rd_num)));
					else
						rs1 := rfile(to_integer(unsigned(rs1_num)));					
					end if;				
				end if;	
				if (fw_res_in.rs2_fwd = '1') then
					rs2 := fw_res_in.rs2;
				else
					rs2 := rfile(to_integer(unsigned(rs2_num)));
				end if;											
				--dohvati rs1 i rs2
			
				--odradi operacije za mov i movi
				if (opcode = mov_ins) or (opcode = movi_ins) or (opcode = push_ins) then
					if (opcode = mov_ins) or (opcode = push_ins) then
						rd := rs1;					
					elsif (opcode = movi_ins) then						
						rd(15 downto 0) := ir(15 downto 0);
					end if;
					rd_operation_done := true;
				else
					rd_operation_done := false;
				end if;			
				--odradi operacije za mov i movi
						
				--dodaj u izlaz za ex		
				id_ex := if_id_in;
				id_ex.opcode := opcode;			
				id_ex.rd_num := rd_num;
				id_ex.rs1_num := rs1_num;
				id_ex.rs2_num := rs2_num;
				if (opcode = store_ins) or branch then
					id_ex.imm := imm_stbr;
				else
					id_ex.imm := imm_ldarlogj;
				end if;
					id_ex.imm_rot := imm_rot;
				
				id_ex.rd := rd;		
				id_ex.rs1 := rs1;
				id_ex.rs2 := rs2;				
				if rd_used then
					id_ex.rd_used := '1';					
				end if;
				if rd_operation_done then
					id_ex.rd_done := '1';
				end if;	
				if (id_flush_in = '1') then
					id_ex.flush := '1';
				end if;
				id_ex_out <= id_ex;
				--dodaj u izlaz za ex							
				
				--dodaj u izlaz za fw
				id_ex_fw_out <= get_forward(id_ex);				
				--dodaj u izlaz za fw
			end if;
		end if;													
	end process;
	
	--WB
	process(clk_id)
	begin
		if rising_edge(clk_id) then			
			if (wb_in.valid = '1' and wb_in.flush /= '1') then
				if (wb_in.rd_used = '1') then
					rfile(to_integer(unsigned(wb_in.rd_num))) <= wb_in.rd;
				end if;
			end if;
		end if;
	end process;
	--WB

end instruction_decode_impl;
