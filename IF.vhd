library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.datatypes_package.all;

entity instruction_fetch is
	port
	(
		clk_if: in std_logic;
		reset_if: in std_logic;
		end_if: in std_logic;
				
		--memorija
		imem_in: out insmem_key;
		imem_out: in insmem_value;
		--memorija
		
		--izlazi
		if_id_out: out interphase;
		if_id_fw_out: out id_forward;
		if_predict_outp: out prediction_key;
		--izlazi
		
		--kontrola spolja
		if_predict_inp: in prediction_value;
		if_jmpjsrbr_addr: in jumpjsrbr_address;
		if_rts_addr: in rts_address;
		if_stall : in std_logic
		--kontrola spolja		
	);
	
end instruction_fetch;

architecture instruction_fetch_impl of instruction_fetch is
	
	function init_pc(filename: string) return std_logic_vector is	
	file file_pointer : text;
	variable line_read : line;	
	variable vec: word;
	begin	
		file_open(file_pointer, filename, READ_MODE);   
		for i in 1 to 5000 loop	
			
			if (endfile(file_pointer)) then
				exit;
			end if;
			
			readline(file_pointer, line_read);
			
			if (line_read(1) = '*') or (line_read(1) = '/') or (line_read(1) = HT) then
				next;			
			end if;
			
			hread(line_read, vec);
			exit;
		end loop;
		return vec;
	end function;
		
	shared variable pc_reset: word := init_pc(inst_in_test);
	shared variable pc_old: word;
	
signal pc : word;
shared variable start_ff: std_logic := '0';
shared variable clk_memory: std_logic := '0';

begin
	process(clk_if)		
	begin
		if rising_edge(clk_if) then
			if (end_if = '1') then
				start_ff := '0';	
			end if;				
			if reset_if = '1' then								
				start_ff := '1';	
				imem_in.rd <= '0';
				pc <= pc_reset;
				pc_old := pc_reset;
			elsif start_ff = '1' then				
				imem_in.rd <= '1';
				if (if_stall = '1') then
					pc <= pc;
					imem_in.addr <= pc_old;
				elsif (if_jmpjsrbr_addr.is_jump = '1') then										
					pc_old := if_jmpjsrbr_addr.jaddr;
					imem_in.addr <= if_jmpjsrbr_addr.jaddr;									
				elsif (if_rts_addr.is_rts = '1') then										
					pc_old := if_rts_addr.rts;
					imem_in.addr <= if_rts_addr.rts;											
				elsif (if_predict_inp.pred_made = '1') and (if_predict_inp.pred_taken = '1') then										
					pc_old := if_predict_inp.pc_pred;
					imem_in.addr <= if_predict_inp.pc_pred;										
				else
					pc_old := pc;
					imem_in.addr <= pc;					
				end if;									
				if (if_stall /= '1') then
					pc <= std_logic_vector(unsigned(pc_old) + 1);
				end if;
			end if;
			if start_ff = '1' and reset_if /= '1' then					
				imem_in.clk <= clk_memory;
				clk_memory := not clk_memory;				
			end if;
		end if;		
	end process;
	
	process(imem_out.done)
	begin		
		if_id_out <= initialize_interphase;
		if_id_fw_out.rs1_num <= B"00000";
		if_id_fw_out.rs2_num <= B"00000";
		if_id_fw_out.enable <= '0';
		if_predict_outp.enable <= '0';
		if_predict_outp.pc <= X"FFFFFFFF";			

		if start_ff = '1' then					
			--odradi ID signale				
			if_id_out.valid <= '1';
			if_id_out.pc <= pc_old;
			if_id_out.pc_next <= std_logic_vector(unsigned(pc));						
			if_id_out.pc_pred <= if_predict_inp.pc_pred;
			if_id_out.pred_made <= if_predict_inp.pred_made;
			if_id_out.pred_taken <= if_predict_inp.pred_taken;
			if_id_out.ir <= imem_out.data;									
			--odredi ID signale
			
			--odredi forward signale
			if_id_fw_out.enable <= '1';			
			if (imem_out.data(31 downto 29) = B"011" or imem_out.data(31 downto 26) = B"000101") then
				if_id_fw_out.rs1_num <= imem_out.data(25 downto 21); --ako shift ili movi
			else
				if_id_fw_out.rs1_num <= imem_out.data(20 downto 16);
			end if;
			if_id_fw_out.rs2_num <= imem_out.data(15 downto 11);
			--odredi forward signale				
			
			--odredi predikciju
			if_predict_outp.enable <= '1';			
			if_predict_outp.pc <= pc_old;
			--odredi predikciju													
		end if;		
	end process;

end instruction_fetch_impl;