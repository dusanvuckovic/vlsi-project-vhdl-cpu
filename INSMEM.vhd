library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
use ieee.std_logic_textio.all;
use work.datatypes_package.all;

entity instruction_memory is
	port (		
		imem_in: in insmem_key;
		imem_out: out insmem_value
	);
end instruction_memory;

architecture instruction_memory_impl of instruction_memory is
	
	constant INSMEM_SIZE: integer := 2**16;
	
	type INSTRUCTION_MEMORY is array (0 to INSMEM_SIZE-1) of word;
	type file_read is file of character;
	
	function init_ram(filename: string) return INSTRUCTION_MEMORY is	
	file file_pointer : text;
	variable line_read : line;
	variable address, data: word;
	variable ram_to_return : INSTRUCTION_MEMORY;	
	variable is_pc: boolean := true;	
	variable bvector: bit_vector(31 downto 0);
	begin	
		file_open(file_pointer, filename, READ_MODE);   
		for i in INSTRUCTION_MEMORY'range loop	
			ram_to_return(i) := X"ffffffff";
		end loop;
		
		for i in INSTRUCTION_MEMORY'range loop				   
		
			if endfile(file_pointer) then 
				exit;
				end if;
			
			readline(file_pointer, line_read);
			
			if (line_read(1) = '*') or (line_read(1) = '/') or (line_read(1) = HT) then
				next;			
			end if;
			
			if is_pc then 
				is_pc := false;
				next;
			end if;
			
			hread(line_read, address);
			
			read(line_read, bvector);
			data := to_stdlogicvector(bvector);
					
			ram_to_return(to_integer(unsigned(address))) := data;
			
		end loop;	
		file_close(file_pointer);
		return ram_to_return;
	end function;

	signal insmem: INSTRUCTION_MEMORY := init_ram(inst_in_test);
	shared variable mem_done: std_logic := '0';                                    
	
begin	
	process(imem_in.clk)
	variable init : boolean := false;
	begin				
		if imem_in.rd = '1' then
			imem_out.data <= insmem(to_integer(unsigned(imem_in.addr)));													
		end if;
		imem_out.done <= mem_done;
		mem_done := not mem_done;
	end process;	
end instruction_memory_impl;

