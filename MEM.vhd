library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.datatypes_package.all;

entity memory is
	port
	(
		clk_mem: in std_logic;
		reset_mem: in std_logic;
		end_mem: out std_logic;
		end_mem_int: out std_logic;

		ex_mem_in: in interphase;
		mem_wb_out: out interphase;
		
		mem_flush_in: in std_logic;
		mem_flush_out: out std_logic;
		
		dmem_in: in datamem_value;
		dmem_out: out datamem_key;		
				
		mem_wb_rts_out: out rts_address;		
		mem_wb_fw_out: out exmemwb_forward
	);
	
end memory;

architecture memory_impl of memory is

signal sp: word := std_logic_vector(to_unsigned(2**16-1, 32));
shared variable clk_m: std_logic := '0';
shared variable is_read: boolean := false;
shared variable is_accessed: boolean := false;
shared variable ex_mem_retain: interphase;
			
begin

	process(clk_mem)		
	begin
		if rising_edge(clk_mem) then
			dmem_out.clk <= clk_m;
			clk_m := not clk_m;
			ex_mem_retain := ex_mem_in;		
			is_accessed := false;
			is_read := false;		
			dmem_out.rd <= '0';
			dmem_out.wr <= '0';
			if (ex_mem_in.valid /= '0' and ex_mem_in.flush /= '1' and mem_flush_in /= '1') then				
				if (ex_mem_in.opcode = load_ins) or (ex_mem_in.opcode = pop_ins) or
				(ex_mem_in.opcode = store_ins) or (ex_mem_in.opcode = push_ins) or
				(ex_mem_in.opcode = jsr_ins) or (ex_mem_in.opcode = rts_ins) or (ex_mem_in.opcode = halt_ins) then
					is_accessed := true;
					if (ex_mem_in.opcode = load_ins) then								
						dmem_out.addr <= ex_mem_in.rd;
						dmem_out.rd <= '1';
						is_read := true;						
					elsif (ex_mem_in.opcode = pop_ins) or (ex_mem_in.opcode = rts_ins) then
						sp <= std_logic_vector(unsigned(sp) + 1);
						dmem_out.addr <= std_logic_vector(unsigned(sp) + 1);
						dmem_out.rd <= '1';
						is_read := true;
					elsif (ex_mem_in.opcode = store_ins) then
						dmem_out.addr <= ex_mem_in.rd;					
						dmem_out.data <= ex_mem_in.rs2;
						dmem_out.wr <= '1';						
					elsif (ex_mem_in.opcode = push_ins) or (ex_mem_in.opcode = jsr_ins) then
						dmem_out.addr <= sp;
						sp <= std_logic_vector(unsigned(sp) - 1);
						dmem_out.data <= ex_mem_in.rd;						
						dmem_out.wr <= '1';
					elsif (ex_mem_in.opcode = halt_ins) then
						end_mem <= '1';
						end_mem_int <= '1';						
					end if;
				end if;			
			end if;		
		end if;
	end process;	
	
	process(dmem_in.done)	
	variable mem_wb: interphase;
	begin	
		mem_wb := ex_mem_retain;
		if (ex_mem_retain.valid /= '0' and ex_mem_retain.flush /= '1' and mem_flush_in /= '1') then
			if is_accessed then
				mem_wb.rd_done := '1';
			end if;
			if is_read then
				mem_wb.rd := dmem_in.data;
			end if;					
		end if;		
		--ako je validna instrukcija
		if (ex_mem_retain.valid /= '0' and ex_mem_retain.flush /= '1' and mem_flush_in /= '1') then
			mem_wb_out <= mem_wb; --izlaz
			mem_wb_fw_out <= get_forward(mem_wb); --forwarding vrednosti (iz izlaza)
			if (mem_wb.opcode = rts_ins) then --skok u IF i flush-ovi
				mem_wb_rts_out.is_rts <= '1';
				mem_wb_rts_out.rts <= dmem_in.data;				
				mem_flush_out <= '1';
			else
				mem_wb_rts_out.is_rts <= '0';
				mem_wb_rts_out.rts <= X"FFFFFFFF";
				mem_flush_out <= '0';
			end if;			
		else
			mem_wb_out <= initialize_interphase;
			mem_wb_fw_out <= get_blocked_forward;			
			mem_wb_rts_out.is_rts <= '0';
			mem_wb_rts_out.rts <= X"FFFFFFFF";
			mem_flush_out <= '0';
		end if;
		if (ex_mem_retain.opcode = halt_ins) then
			mem_flush_out <= '1';
		end if;
	
	end process;

end memory_impl;