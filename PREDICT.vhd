library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.datatypes_package.all;

entity predict is	
	port (
		clk_pred: in std_logic;
		reset_pred: in std_logic;	
	
		if_pred_inp: in prediction_key;
		if_pred_outp: out prediction_value;
		ex_mem_pred_upd: in prediction_update
	);
end predict;

architecture predict_impl of predict is
	
	constant PREDICTION_BUFFER_SIZE: integer := 4;		
	
	type prediction_row is
	record
		pc: word;
		pc_pred: word;
		pred_state: std_logic_vector(1 downto 0);
		second_chance: std_logic;
	end record;
	
	type prediction_buffer is array (0 to PREDICTION_BUFFER_SIZE-1) of prediction_row;

	function init_pred_buffer return prediction_buffer is
		variable pb: prediction_buffer;		
	begin
		for i in 0 to PREDICTION_BUFFER_SIZE-1 loop
			pb(i).pc := X"FFFFFFFF";
			pb(i).pc_pred := X"FFFFFFFF";
			pb(i).pred_state := B"11";
			pb(i).second_chance := '0';
		end loop;
		return pb;
	end init_pred_buffer;
	
	signal pbuf: prediction_buffer := init_pred_buffer;
	signal pbuf_pointer : integer range 0 to PREDICTION_BUFFER_SIZE := 0;
	
	function initialize_value return prediction_value is
		variable v: prediction_value;
	begin
		v.pred_made := '0';
		v.pred_taken := '0';
		v.pc_pred := X"FFFFFFFF";	
		return v;
	end initialize_value;
	
begin
	process(clk_pred)
	variable v: prediction_value;
	variable found: boolean := false;
	variable pb: prediction_buffer;
	variable pb_ptr: integer range 0 to PREDICTION_BUFFER_SIZE;
	begin
		if falling_edge(clk_pred) then
		found := false;
		pb := pbuf;
		pb_ptr := pbuf_pointer;
		if (reset_pred = '1') then
			for i in 0 to PREDICTION_BUFFER_SIZE-1 loop
				pb(i).second_chance := '0';
			end loop;		
			pb_ptr := 0;
		elsif (ex_mem_pred_upd.pred_ins = '1') then
			for i in 0 to PREDICTION_BUFFER_SIZE-1 loop
				if (pb(i).pc = ex_mem_pred_upd.pc_entry) then
					found := true;					
					if (ex_mem_pred_upd.pred_calc = '1') and (pb(i).pred_state /= B"11") then
						pb(i).pred_state := std_logic_vector(unsigned(pb(i).pred_state) + 1);							
					end if;
					if (ex_mem_pred_upd.pred_calc = '0') and (pb(i).pred_state /= B"00") then
						pb(i).pred_state := std_logic_vector(unsigned(pb(i).pred_state) - 1);							
					pb(i).second_chance := '1';
					end if;
				end if;
			end loop;
			if not found then
				for i in 1 to 2*pb'high loop				
					if (pb(pb_ptr).second_chance = '1') then
						pb(pb_ptr).second_chance := '0';
						pb_ptr := (pb_ptr + 1) mod PREDICTION_BUFFER_SIZE;
					else
						pb(pb_ptr).pc := ex_mem_pred_upd.pc_entry;
						pb(pb_ptr).pc_pred := ex_mem_pred_upd.pc_calc;
						if (ex_mem_pred_upd.pred_calc = '1') then
							pb(pb_ptr).pred_state := B"10";
						else
							pb(pb_ptr).pred_state := B"01";
						end if;
						pb(pb_ptr).second_chance := '1';
						pb_ptr := (pb_ptr + 1) mod PREDICTION_BUFFER_SIZE;
						exit;
					end if;										
				end loop;						
			end if;		
		end if;
		v := initialize_value;
		if (if_pred_inp.enable = '1') then
			for i in 0 to PREDICTION_BUFFER_SIZE-1 loop
				if (pb(i).pc = if_pred_inp.pc) then
					v.pred_made := '1';
					v.pred_taken := pb(i).pred_state(1);
					v.pc_pred := pb(i).pc_pred;
					exit;
				end if;
			end loop;
		end if;
		if_pred_outp <= v;
		pbuf <= pb;
		pbuf_pointer <= pb_ptr;		
		end if;
	end process;
	
end predict_impl;