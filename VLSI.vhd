library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_textio.all;
use std.textio.all;
use work.datatypes_package.all;

entity vlsi is	
	port (
		reset: in std_logic;
		clk: in std_logic
	);
end vlsi;

architecture vlsi_cpu of vlsi is

signal imem_key: insmem_key;
signal imem_value: insmem_value;

signal dmem_key: datamem_key;
signal dmem_value: datamem_value;

signal end_sim: std_logic := '0';

	begin			
		
	insmem: entity work.instruction_memory(instruction_memory_impl)
		port map (	
			imem_in => imem_key,
			imem_out => imem_value
	);
	
	datamem: entity work.data_memory(data_memory_impl)
		port map (	
			dmem_in => dmem_key,
			dmem_out => dmem_value,
			dmem_end => end_sim
	);
	
	cpu_vlsi: entity work.cpu(cpu_impl)
		port map (
			clk_cpu => clk,
			reset_cpu => reset,
			end_cpu => end_sim,
			
			imem_key => imem_key,					
			imem_value => imem_value,
			
			dmem_key => dmem_key,
			dmem_value => dmem_value
	);
				
end vlsi_cpu;


