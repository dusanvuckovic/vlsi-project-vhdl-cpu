LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY vlsi_vhd_tst IS
END vlsi_vhd_tst;
ARCHITECTURE vlsi_arch OF vlsi_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL clk : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
COMPONENT vlsi
	PORT (
	clk : IN STD_LOGIC;
	reset : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	cpu : vlsi
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	reset => reset
	);
	
PROCESS                                              
variable clk_next: STD_LOGIC := '0';                                    
BEGIN    
	clk <= clk_next;
	clk_next := not clk_next;
	reset <= '1';
	wait for 5 ns;
	clk <= clk_next;
	clk_next := not clk_next;
	wait for 5 ns;
	reset <= '0';
	loop
		clk <= clk_next;
		clk_next := not clk_next;
		wait for 5 ns;
	end loop;
WAIT;                                                        
END PROCESS;                                          
END vlsi_arch;
